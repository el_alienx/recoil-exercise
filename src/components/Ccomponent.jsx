import React from "react";

// Recoil library (this one replaces the normal React Hook)
import { useRecoilState } from "recoil";

// Global stage
import { textState, countState } from "../state/information";

export default function Ccomponent() {
  const [text, setText] = useRecoilState(textState);
  const [count, setCount] = useRecoilState(countState);

  return (
    <section>
      <hr />
      <h2>C COMPONENT {text}</h2>
      <button onClick={() => setCount(500)}>CLICK ME! {count}</button>
      <hr />
    </section>
  );
}
