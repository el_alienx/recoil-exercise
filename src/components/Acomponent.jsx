import React from "react";

// Recoil library
import { useRecoilState } from "recoil";

// Global component
import { countState } from "../state/information";

export default function Acomponent() {
  const [count] = useRecoilState(countState);

  return (
    <article>
      <h2>Hello from A component</h2>
      <p>{count}</p>
    </article>
  );
}
