import React from "react";

// Recoil library
import { useRecoilState } from "recoil";

// Global state
import { textState } from "../state/information";

export default function Bcomponent() {
  const [text] = useRecoilState(textState);

  return (
    <div>
      <p>
        Lorem ipsum dolor <strong>{text}</strong> sit amet consectetur,
        adipisicing elit. Iusto saepe quos officiis vero at alias accusantium
        qui libero, accusamus quae quo corrupti, iure rem dolor, tempore
        reiciendis! Non, molestiae ipsam?
      </p>
    </div>
  );
}
