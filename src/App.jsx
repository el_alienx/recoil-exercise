import { RecoilRoot } from "recoil";

import "./style.css";

import Acomponent from "./components/Acomponent";
import Bcomponent from "./components/Bcomponent";
import Ccomponent from "./components/Ccomponent";

function App() {
  return (
    <RecoilRoot>
      <div className="App">
        <h1>Recoil Exercise</h1>
        <div className="grid">
          <Acomponent />
          <Bcomponent />
          <Ccomponent />
        </div>
      </div>
    </RecoilRoot>
  );
}

export default App;
