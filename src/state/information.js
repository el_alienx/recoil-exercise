import { atom } from "recoil";
// Atom is our indvidual piece of reactive data

export const textState = atom({
  key: "textState",
  default: "hello",
});

export const countState = atom({
  key: "countState",
  default: 0,
});
